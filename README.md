# Tunnels

Given a list of interconnected tunnel system, with start point, end point & max cars that can pass through per hour, calculates the max cars that can pass between any two start & end points

## Features

- Gets all possible routes and max cars for each route
- Gets max cars that can pass between source and destination

## Installation

### Prerequisite

[Node.js](https://nodejs.org/) v14+
[NPM](https://www.npmjs.com/) v7+

Install the dependencies and start the app

```sh
npm ci
npm start
```

## Usage

To test with different datasets, update the following file

```
src/config/data.json
```

source: start point
destination: end point
routes: array of arrays where each array item denotes the start point, end points and max car per hour

## License

UNLICENSED
