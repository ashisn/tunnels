import Tunnels from './tunnels';
import { source, destination, routes } from './config/data.json';

try {
  const tunnels = new Tunnels(source, destination, routes);

  console.info(
    `Maximum cars that can pass through ${source} and ${destination} is: ${tunnels.maxCarLimit}`
  );
} catch (e) {
  console.error(`Error while getting max car limits: ${e?.message}`);
}
