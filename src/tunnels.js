import validate from './util/validate';

export default class Tunnels {
  #allRoutesWithCarLimit = [];
  #edges = [];
  #isVisited = [];
  #pathList = [];
  #possibleRoutes = [];
  #routesCount = 0;
  #routeWithCarLimit = {};

  constructor(source, destination, tunnels) {
    this.source = source;
    this.destination = destination;
    this.tunnels = tunnels;
    this.#validate();
    this.#setUp();
  }

  #hasSourceAndDestination(prev, [src, dst]) {
    return [
      prev[0] || src === this.source,
      prev[1] || dst === this.destination,
    ];
  }

  #validate() {
    const { source, destination, tunnels } = this;
    validate({ source, destination, tunnels })

    if (
      !this.tunnels
        .reduce(
          (prev, curr) => this.#hasSourceAndDestination(prev, curr),
          [0, 0]
        )
        .every(Boolean)
    ) {
      throw new Error(
        `Tunnels data needs to have at least one entry each for source & destination! Supplied value is, tunnels: ${this.tunnels}`
      );
    }
  }

  #setUp() {
    this.tunnels.forEach((tunnel) => void this.#addEdgeWithWeight(tunnel));
    this.#pathList.push(this.source);
  }

  #addEdgeWithWeight([vertex1, vertex2, weight]) {
    if (!this.#edges[vertex1]) {
      this.#edges[vertex1] = [];
    }

    this.#edges[vertex1].push({ destination: vertex2, weight });
  }

  #populateRoutesAndCarsList = (src, dst, isVisited, localPathList) => {
    if (src == dst) {
      this.#possibleRoutes.push([...localPathList]);
      this.#routeWithCarLimit[src] = 0;

      return;
    }

    isVisited[src] = true;

    this.#edges[src]?.forEach((edge) => {
      const { destination, weight } = edge;

      if (!isVisited[destination]) {
        localPathList.push(destination);

        if (this.#routeWithCarLimit[src]) {
          this.#allRoutesWithCarLimit[this.#routesCount++] = {
            ...this.#routeWithCarLimit,
          };
        }

        this.#routeWithCarLimit[src] = weight;
        this.#populateRoutesAndCarsList(
          destination,
          dst,
          isVisited,
          localPathList
        );
        localPathList.splice(localPathList.indexOf(destination), 1);
      }
    });

    isVisited[src] = false;
  };

  get routesAndCarsLimit() {
    this.#populateRoutesAndCarsList(
      this.source,
      this.destination,
      this.#isVisited,
      this.#pathList
    );

    if (this.#possibleRoutes.length !== this.#allRoutesWithCarLimit.length) {
      this.#allRoutesWithCarLimit[this.#routesCount] = {
        ...this.#routeWithCarLimit,
      };
    }

    if (!this.#possibleRoutes.length) {
      this.#allRoutesWithCarLimit = [];
    }

    return {
      possibleRoutes: this.#possibleRoutes,
      allRoutesWithCarLimit: this.#allRoutesWithCarLimit,
    };
  }

  get maxCarLimit() {
    const { possibleRoutes, allRoutesWithCarLimit } = this.routesAndCarsLimit;

    return (
      possibleRoutes
        .map((path, index) => {
          const selectedRouteCarsList = allRoutesWithCarLimit[index];
          const carsCount = path.reduce(
            (prev, curr) => prev + selectedRouteCarsList[curr],
            0
          );

          return carsCount;
        })
        .sort((c1, c2) => c2 - c1)[0] || 0
    );
  }
}
