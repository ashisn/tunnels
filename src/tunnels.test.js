import inValidRoutes from './test-data/invalid-routes.json';
import inValidRoutesResponse from './test-data/invalid-routes-response.json';
import validRoutes from './test-data/valid-routes.json';
import validRoutesResponse from './test-data/valid-routes-response.json';
import Tunnels from './tunnels';

describe('tunnels', () => {
  describe('routesAndCarsLimit', () => {
    test('returns all possible routes and correspponding cars count when source to destination route found', () => {
      const tunnels = new Tunnels('source', 'destination', validRoutes);

      expect(tunnels.routesAndCarsLimit).toStrictEqual(validRoutesResponse);
    });

    test('returns empty arrays for all possible routes and correspponding cars count when no source to destination route found', () => {
      const tunnels = new Tunnels('source', 'destination', inValidRoutes);

      expect(tunnels.routesAndCarsLimit).toStrictEqual(inValidRoutesResponse);
    });
  });

  describe('maxCarLimit', () => {
    test('returns max cars count when source to destination route found', () => {
      const tunnels = new Tunnels('source', 'destination', validRoutes);

      expect(tunnels.maxCarLimit).toEqual(100);
    });

    test('returns cars count as 0 when no source to destination route found', () => {
      const tunnels = new Tunnels('source', 'destination', inValidRoutes);

      expect(tunnels.maxCarLimit).toStrictEqual(0);
    });
  });

  describe('validate', () => {
    describe('valid', () => {
      test('should not throw error', () => {
        expect(() => {
          new Tunnels('source', 'destination', [['source', 'destination', 50]]);
        }).not.toThrowError();
      });
    });

    describe('invalid (throw error)', () => {
      test('there is not at least one entry each for source and destination in tunnels array', () => {
        expect(() => {
          new Tunnels('source', 'destination', [
            ['s', 'n', 50],
            ['n', 'destination', 10],
          ]);
        }).toThrowError(
          'Tunnels data needs to have at least one entry each for source & destination! Supplied value is, tunnels: s,n,50,n,destination,10'
        );
      });
    });
  });
});
