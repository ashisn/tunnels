import Ajv from 'ajv';

import schema from '../config/schema.json';

const ajv = new Ajv();

export default function (data) {
  if (!ajv.validate(schema, data)) {
    throw new Error(JSON.stringify(ajv.errors));
  }
}