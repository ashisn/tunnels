import validate from './validate';

describe('validate', () => {
  const source = 'source';
  const destination = 'destination';

  describe('valid', () => {
    test('should not throw error', () => {
      expect(() => {
        validate({ source, destination, tunnels: [['source', 'destination', 50]] });
      }).not.toThrowError();
    });
  });

  describe('invalid (throw error)', () => {
    test('missing source param', () => {
      expect(() => {
        validate({
          source: undefined, destination, tunnels: [
            ['source', 'destination', 50],
          ]
        });
      }).toThrowError(
        '[{"instancePath":"","schemaPath":"#/required","keyword":"required","params":{"missingProperty":"source"},"message":"must have required property \'source\'"}]'
      );
    });

    test('missing destination param', () => {
      expect(() => {
        validate({ source, destination: undefined, tunnels: [['source', 'destination', 50]] });
      }).toThrowError(
        '[{"instancePath":"","schemaPath":"#/required","keyword":"required","params":{"missingProperty":"destination"},"message":"must have required property \'destination\'"}]'
      );
    });

    test('missing tunnels param', () => {
      expect(() => {
        validate({ source, destination, tunnels: undefined });
      }).toThrowError(
        '[{"instancePath":"","schemaPath":"#/required","keyword":"required","params":{"missingProperty":"tunnels"},"message":"must have required property \'tunnels\'"}]'
      );
    });

    test('tunnels param not an array', () => {
      expect(() => {
        validate({ source, destination, tunnels: {} });
      }).toThrowError(
        '[{"instancePath":"/tunnels","schemaPath":"#/properties/tunnels/type","keyword":"type","params":{"type":"array"},"message":"must be array"}]'
      );
    });

    test('each item in tunnels array not an array', () => {
      expect(() => {
        validate({
          source, destination, tunnels: [
            ['s', 'n', 50],
            { s: 'start', d: 'end', cars: 50 },
          ]
        });
      }).toThrowError(
        '[{"instancePath":"/tunnels/1","schemaPath":"#/properties/tunnels/items/type","keyword":"type","params":{"type":"array"},"message":"must be array"}]'
      );
    });

    test('each item in tunnels does not contain 3 fields', () => {
      expect(() => {
        validate({
          source, destination, tunnels: [
            ['s', 'n', 50],
            ['n', 'd'],
          ]
        });
      }).toThrowError(
        '[{"instancePath":"/tunnels/1","schemaPath":"#/properties/tunnels/items/minItems","keyword":"minItems","params":{"limit":3},"message":"must NOT have fewer than 3 items"}]'
      );
    });
  });
});